# frozen_string_literal: true

class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.string :firstname
      t.string :lastname
      t.string :address
      t.string :city
      t.string :state
      t.integer :zipcode
      t.string  :image
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
