# frozen_string_literal: true

Rails.application.routes.draw do
  # get 'profile/new'
  # get 'profile/create'
  # post 'profile/create'
  # get 'profile/show'
  # get 'profile/edit'
  # get 'profile/update'
  # get 'profile/destroy'
  # get 'authentication/login'
  # get 'sessions/login'
  # post 'sessions/login'
  # get 'sessions/home'
  # get 'sessions/profile'
  # get 'sessions/settings'
  # post 'users/new'
  # get 'users/create'
  # post 'users/create'
  # get 'users/login'
  get 'authentication/login'
  get 'authentication/signup'
  get 'welcome/index'
  post 'authentication/create'
  get  'authentication/create'
  post 'authentication/authentication_login'
  get 'authentication/destroy'
  post 'authentication/destroy'

  
  resources :profiles

  resources :user do
    resources :articles
  end

  resources :articles do
    resources :comments
  end

  root 'authentication#login'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
