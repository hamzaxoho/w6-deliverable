// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .



$(document).ready(function(){


	$('#usernamecheck').hide();
	$('#passwordcheck').hide();
	$('#text_feild').hide();
	$('#title_feild').hide();
	$('.checkfeilds_error').hide();





	$('.submitform').click(function(e){

		e.preventDefault();

		username_check();
	 	password_check();


	 	

	 	  if($(".check-field:visible").length == 0)
	 	  {


	 	  	$('form').submit();


	 	  }

	 	  	
	 });
	 	

	 	function username_check(){

				var username_val=$('#username').val();


				if(username_val=='')
				{
					$('#usernamecheck').show();
					$('#usernamecheck').html("** Username feild must not be empty");
					$('#usernamecheck').focus();
					$('#usernamecheck').css("color" , "red");

					return false;
				}

				else
				{
					$('#usernamecheck').hide();
				}
	 }

	 function firstname_check(){

				var firstname_val=$('.firstname').val();


				if(firstname_val=='')
				{
					$('#firstname').show();
					$('#firstname').html("**Please Enter your firstname");
					$('#firstname').focus();
					$('#firstname').css("color" , "red");

					return false;
				}

				else
				{
					$('#firstname').hide();
				}
	 }

	 

	 function password_check(){

	 	var password_val=$('#password').val();


				if(password_val=='')
				{
					$('#passwordcheck').show();
					$('#passwordcheck').html("** Password feild must not be empty");
					$('#passwordcheck').focus();
					$('#passwordcheck').css("color" , "red");

					return false;
				}

				else
				{
					$('#passwordcheck').hide();
				}



	 }

	 function address_check(){

	 	var address_val=$('.address').val();


				if(password_val=='')
				{
					$('#addresscheck').show();
					$('#addresscheck').html("** Address feild must not be empty");
					$('#addresscheck').focus();
					$('#addresscheck').css("color" , "red");

					return false;
				}

				else
				{
					$('#addresscheck').hide();
				}



	 }

});
	 	

	

