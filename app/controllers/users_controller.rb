# frozen_string_literal: true

class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save

      flash[:notice] = 'You signed up successfully'

      redirect_to welcome_index_path

    else

      flash[:notice] = 'Form is invalid'

      render 'new'
    end
  end

  def login; end

  private

  def user_params
    params.require(:user).permit(:username, :password, :salt, :encrypted_password)
end
end
