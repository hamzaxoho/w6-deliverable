# frozen_string_literal: true

class ArticlesController < ApplicationController
  http_basic_authenticate_with name: 'hamza', password: '1234', except: %i[index show]

  before_action :authentication_login

  def index
    @user = User.find_by(id: session[:user_id])

    @articles = @user.articles
  end

  # create a new instance variable and Allow to make new article in Article class
  def new
    @article = Article.new
  end

  def edit
    @article = Article.find(params[:id])
  end

  def create
    @article = Article.new(article_params)

    if @article.save

      redirect_to @article
    else

      render 'new'
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if @article.update(article_params)

      redirect_to @article
    else
      render 'edit'
     end
  end

  def destroy
    @article = Article.find(params[:id])

    @article.destroy

    redirect_to articles_path
  end

  private

  def authentication_login
    redirect_to authentication_login_path if session[:user_id].blank?
  end

  def article_params
    params.require(:article).permit(:user_id, :title, :text)
  end
end
