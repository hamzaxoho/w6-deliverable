# frozen_string_literal: true

class AuthenticationController < ApplicationController
  # before_action :authentication_login

  def create
    @user = User.create(username: params[:username], password_digest: params[:password])

    if @user

      session[:user_id] = @user.id

      flash[:notice] = 'You signed up successfully'

      redirect_to articles_path

    else

      flash[:notice] = 'Problem with signup Password feild must not be empty'

      redirect_to authentication_login_path
    end
  end

  def authentication_login
    @user = User.find_by(username: params[:username], password_digest: params[:password])

    if @user

      session[:user_id] = @user.id

      redirect_to articles_path

    else

      flash[:notice] = 'Invalid Username or Password'

      redirect_to authentication_login_path
    end
  end

  def destroy
    session[:user_id] = nil

    redirect_to authentication_login_path, alert: "You've successfully logged out! Login again?"
  end
end
