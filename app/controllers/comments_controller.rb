# frozen_string_literal: true

class CommentsController < ApplicationController
  before_action :authentication_login

  http_basic_authenticate_with name: 'hamza', password: '1234', only: :destroy

  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article)
  end

  private

  def authentication_login
    redirect_to authentication_login_path if session[:user_id].blank?
      end

  def comment_params
    params.require(:comment).permit(:commenter, :body)
  end
end
