# frozen_string_literal: true

class SessionsController < ApplicationController
  def login
    user = User.authenticate(params[:username], params[:password])

    if user

      flash[:notice] = 'Wow Welcome again, you logged in'

      redirect_to Welcome_index_path

    else

      flash[:notice] = 'Invalid Username or Password'

    end
  end

  def home; end

  def profile; end

  def settings; end
end
