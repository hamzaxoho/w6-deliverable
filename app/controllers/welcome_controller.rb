# frozen_string_literal: true

class WelcomeController < ApplicationController
  before_action :authentication_login

  def index
    puts 'My Blog is ready'
  end

  private

  def authentication_login
    if session[:user_id].blank?

      redirect_to authentication_signup_path, alert: 'Problem in Signup Try again'

   end
  end
end
