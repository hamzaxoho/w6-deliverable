# frozen_string_literal: true

class ProfilesController < ApplicationController
  before_action :authentication_login
  # before_action :check_profile_presence, only: [:new, :create]

  def index
    @user = User.find_by(id: session[:user_id])

    @profile = @user.profile
  end

  def new
    @profile = Profile.new
  end

  def create
    @profile = Profile.new(profile_params)

    if @profile.save

      redirect_to @profile

    else

      render 'new'

    end
  end

  def show
    @profile = Profile.find(params[:id])
  end

  def edit
    @profile = Profile.find(params[:id])
  end

  def update
    @profile = Profile.find(params[:id])

    if @profile.update(profile_params)

      redirect_to @profile

    else

      render 'edit'
    end
  end

  private

  def authentication_login
    redirect_to authentication_login_path if session[:user_id].blank?
  end

  def check_profile_presence
    # redirect_to user_profile_path if current_user.profile.exists?
  end

  def profile_params
    params.require(:profile).permit(:user_id, :image, :firstname, :lastname, :address, :city, :state, :zipcode)
  end
end
