# frozen_string_literal: true

class User < ApplicationRecord
  has_one :profile

  has_many :articles

  validates :password_digest, presence: true,
                              length: { within: 6..12 }
end
